# Starship Shop
This is a technical test project by Lou Rectoret in order to get hired into the almighty [SDD Brandcare team](https://www.linkedin.com/company/sdd-brandcare/people/).

## GOAL
The technical test is to develop using VueJS and Vuex a small (very simple) ecommerce where you can buy Star Wars ships.

## CHALLENGES
1. **List All Spaceships View**
    1. List All Spaceships
    2. Buy Now Button
    3. Link to spaceship Detail view
2. **Display a Spaceship Detail view**
    1. Display extended info about the spaceship
    2. Buy button
3. **Shopping Cart View**
    1. List all selected spaceships
    2. Spaceships can be deleted
    3. Spaceships units can be modified
    4. Spaceships show total units
    5. Spaceships show total price in Credits
    6. Show Total price in Credits 

## Requirements
* GIT repo somewhere with  commints
* Unit Testing
* SPA
* Vuex
* Vuetify
* Customize Vuetify default style with SASS
* Whatever extra is a plus

## Source
[Swiapi REST API](https://swapi.co/) will provide spaceship Data

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customized configuration
See files:
- package.json
- .prettierrc
- .eslintrc.js

