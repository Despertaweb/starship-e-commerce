import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'

import { shallowMount } from '@vue/test-utils'

import { ui } from '@/store/modules/ui.js'
import BLoading from '@/components/base/BLoading.vue'
import mixins from '@/mixins/mixins.js'

let wrapper = null

beforeEach(() => {
  Vue.use(Vuetify)
  Vue.use(Vuex)

  const store = new Vuex.Store({
    modules: { MUI: ui },
  })

  wrapper = shallowMount(BLoading, {
    store,
    mixins,
  })
})

afterEach(() => wrapper.destroy())
describe('* Loading Base Component ', () => {
  it('Show Component', async () => {
    const loading = wrapper.find('#loadingWrapper')

    // Set to display the component
    wrapper.vm.$setLoading()

    // Is mounted
    expect(loading.isVisible()).toBe(true)
  })

  it('Hide Component', async () => {
    const loading = wrapper.find('#loadingWrapper')

    // Set to display the component
    wrapper.vm.$unsetLoading()

    // Is NOT mounted
    expect(loading.isVisible()).not.toBe(false)
  })

  /************************* */
  /*      MESSAGE TEXT       */
  /************************* */

  it('Loading Message: Default', () => {
    // Set to Default msg
    wrapper.vm.$setLoading()

    expect(wrapper.vm.loading.msg).toBe('Loading...')
  })
  it('Loading Message: "Hello Dart"', () => {
    // Set to Default msg
    const msg = 'Hello Dart'
    wrapper.vm.$setLoading(msg)

    expect(wrapper.vm.loading.msg).toBe(msg)
  })

  describe('\n  *SIDE NOTE:  @mixins/mixins.js are tested along with the component', () => {
    it('', () => {})
  })
})
