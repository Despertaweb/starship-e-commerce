// Import Module Store to be tested
import { shoppingCart } from '@/store/modules/shopping-cart.js'

const { mutations, getters } = shoppingCart

const mockedItems = [
  { name: 'Death Star', cost_in_credits: 23, count: 1 },
  { name: 'Executor', cost_in_credits: 2, count: 2 },
]

describe('Vuex Shopping Cart Module\n\n', () => {
  describe('MUTATIONS:', () => {
    describe('PUSH_ITEM', () => {
      it(' - item at empty cart', () => {
        const state = {
          items: [],
        }

        expect(state.items).toHaveLength(0)
        mutations.PUSH_ITEM(state, { ...mockedItems[0] })
        expect(state.items).toHaveLength(1)
        expect(state.items).toContainEqual(mockedItems[0])
      })

      it(' - non added item cart', () => {
        const state = {
          items: [{ name: 'Executor', cost_in_credits: 2, count: 2 }],
        }

        expect(state.items).toHaveLength(1)
        mutations.PUSH_ITEM(state, { ...mockedItems[0] })
        expect(state.items).toHaveLength(2)
      })

      it(' - increment counter', () => {
        const state = {
          items: [
            { name: 'Death Star', cost_in_credits: 23, count: 1 },
            { name: 'Executor', cost_in_credits: 2, count: 2 },
          ],
        }
        mutations.PUSH_ITEM(state, { ...mockedItems[1] })
        expect(state.items.length).toBe(mockedItems.length)

        expect(state.items).not.toContainEqual(
          expect.objectContaining({
            ...mockedItems[1],
            count: mockedItems[1].count++,
          }),
        )
      })
    })

    describe('\n     DELETE_ITEM', () => {
      // Removes all the items of a model from the cart
      it('Delete Item from cart', () => {
        const state = {
          items: [
            { name: 'Death Star', cost_in_credits: 23, count: 1 },
            { name: 'Executor', cost_in_credits: 2, count: 2 },
            {
              name: 'Nebulon-B escort frigate',
              cost_in_credits: 8500,
              count: 3,
            },
          ],
        }
        const removedItem = {
          name: 'Nebulon-B escort frigate',
          cost_in_credits: 8500,
          count: 3,
        }

        expect(state.items).toHaveLength(3)

        mutations.DELETE_ITEM(state, removedItem)

        expect(state.items).toHaveLength(2)
        expect(state.items).not.toContainEqual(
          expect.objectContaining(removedItem),
        )
      })
    })

    describe('\n      REST_ITEM', () => {
      // Removes 1 Death Star's model from the cart
      it('Decrease Item count in 1', () => {
        const state = {
          items: [
            { name: 'Death Star', cost_in_credits: 66, count: 4 },
            { name: 'Executor', cost_in_credits: 66, count: 1 },
          ],
        }
        const itemToDecrease = state.items[0]

        expect(state.items).toHaveLength(2)
        mutations.REST_ITEM(state, itemToDecrease)
        expect(state.items).toHaveLength(2)

        // Check count is count -1
        expect(state.items).not.toContainEqual(
          expect.objectContaining({
            ...itemToDecrease,
            count: itemToDecrease.count--,
          }),
        )
      })
      // Removes the whole bunch items of a model from the cart
      it('Delete Items of model Executor', () => {
        const state = {
          items: [
            { name: 'Death Star', cost_in_credits: 66, count: 4 },
            { name: 'Executor', cost_in_credits: 66, count: 1 },
          ],
        }
        const itemToDecrease = state.items[1]

        expect(state.items).toHaveLength(2)
        mutations.REST_ITEM(state, itemToDecrease)
        expect(state.items).toHaveLength(1)

        // Check count is count -1
        expect(state.items).not.toContain(
          expect.objectContaining({
            ...itemToDecrease,
            name: 'Executor',
          }),
        )
      })
    })
  })

  describe('GETTERS:', () => {
    it('Count total number of items ', () => {
      const state = {
        items: [
          { name: 'Death Star', cost_in_credits: 66, count: 4 },
          { name: 'Executor', cost_in_credits: 66, count: 1 },
        ],
      }

      // Expected => 4 + 1 = 5
      const itemsTotal = getters.itemsCount(state)

      expect(itemsTotal).toBe(5)
    })
  })
})
