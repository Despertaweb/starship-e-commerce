import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'

import { shallowMount } from '@vue/test-utils'

import { ui } from '@/store/modules/ui.js'
import BSnackbar from '@/components/base/BSnackbar.vue'
import mixins from '@/mixins/mixins.js'

let wrapper = null

beforeEach(() => {
  Vue.use(Vuetify)
  Vue.use(Vuex)

  const store = new Vuex.Store({
    modules: { MUI: ui },
  })

  wrapper = shallowMount(BSnackbar, {
    store,
    mixins,
  })
})

afterEach(() => wrapper.destroy())
describe('* Snackbar Base Component ', () => {
  it('Show Component', () => {
    let snackbarWrapper = wrapper.vm.$store.state.MUI.snackbar

    expect(wrapper.find('#snackbar').exists()).toBe(true)
    expect(snackbarWrapper.text).not.toBe('Hello Dart')

    wrapper.vm.$setSnackbar('Hello Dart', '22000', 'green')
    snackbarWrapper = wrapper.vm.$store.state.MUI.snackbar
    expect(snackbarWrapper.text).toBe('Hello Dart')
  })

  describe('\n  *SIDE NOTE:  ', () => {
    it('@mixins/mixins.js are tested along with the component', () => {})
  })
})
