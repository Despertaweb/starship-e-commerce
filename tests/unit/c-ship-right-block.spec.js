import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'

import { shallowMount } from '@vue/test-utils'

import router from '@/router'
import { shoppingCart } from '@/store/modules/shopping-cart.js'
import VShipRightBlock from '@/components/ship/ShipRightBlock.vue'

let wrapper = null

beforeEach(() => {
  Vue.use(Vuetify)
  Vue.use(Vuex)

  const store = new Vuex.Store({
    modules: { MShoppingCart: shoppingCart },
  })
  store.commit = jest.fn()

  wrapper = shallowMount(VShipRightBlock, {
    store,
    router,
    propsData: {
      shipData: {
        name: 'Name Ship',
        model: 'Last Model Fashion Ship',
        cost_in_credits: '1349',
        hyperdrive_rating: 4,
        films: [
          'https://swapi.co/api/films/2/',
          'https://swapi.co/api/films/7/',
          'https://swapi.co/api/films/3/',
          'https://swapi.co/api/films/1/',
        ],
        pilots: [
          'https://swapi.co/api/pìlots/2/',
          'https://swapi.co/api/pìlots/7/',
          'https://swapi.co/api/pìlots/1/',
        ],
        url: 'https://swapi.co/api/starships/9/',
      },
    },
  })
})

afterEach(() => wrapper.destroy())

describe('VShoppingcart view', () => {
  describe('Render props in view', () => {
    it('renders a list of stars equal to hyperdrive_rating', () => {
      const ratingStars = wrapper.find('#rating').props('value')

      expect(ratingStars).toBe(wrapper.vm.shipData.hyperdrive_rating)
    })

    it('Name', () => {
      const name = wrapper.find('#name').text()

      expect(name).toEqual(wrapper.vm.shipData.name)
    })

    it('Model text', () => {
      const model = wrapper.find('#model').text()

      expect(model).toEqual(wrapper.vm.shipData.model)
    })

    it('Films count - computed property', () => {
      const filmsCountText = wrapper.find('#filmsCount').text()
      expect(filmsCountText).toEqual(
        wrapper.vm.shipData.films.length.toString(),
      )

      const filmsCount = wrapper.vm.filmsCount
      expect(filmsCount).toEqual(wrapper.vm.filmsCount)
    })

    it('Pilots count - computed property', () => {
      const shipPilotsCountText = wrapper.find('#pilotsCount').text()
      expect(shipPilotsCountText).toEqual(
        wrapper.vm.shipData.pilots.length.toString(),
      )

      const shipPilotsCount = wrapper.vm.pilotsCount
      expect(shipPilotsCount).toEqual(wrapper.vm.pilotsCount)
    })

    it('Credits = credits / 1000 - computed property', () => {
      const credits = wrapper.find('#credits').text()

      expect(credits).toMatch(wrapper.vm.credits)
    })
  })

  describe('User Interaction', () => {
    it('Add Item to cart button', async () => {
      const addToCartBtn = wrapper.find('#addToCartBtn')

      // The btn exists
      expect(addToCartBtn.exists()).toBe(true)

      // Click
      addToCartBtn.trigger('click')
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$store.commit).toHaveBeenCalled()
    })

    it('Buy now button', async () => {
      // The btn exists
      const buyNowBtn = wrapper.find('#buyNowBtn')
      expect(buyNowBtn.exists()).toBe(true)

      buyNowBtn.trigger('click')
      await buyNowBtn.vm.$nextTick()

      // Trigger commit
      expect(wrapper.vm.$store.commit).toHaveBeenCalled()

      // New route
      expect(wrapper.vm.$route.name).toMatch('cart')
    })
  })
})
