import { mount, RouterLinkStub } from '@vue/test-utils'
import Vue from 'vue'
import vuetify from 'vuetify'
import VHome from '@/views/VHome.vue'

Vue.use(vuetify)

describe('VHome', () => {
  const wrapper = mount(VHome, {
    stubs: {
      RouterLink: RouterLinkStub,
    },
  })

  it(' - Home Has a link to Ships', () => {
    expect(wrapper.find(RouterLinkStub).props().to).toStrictEqual({
      name: 'ships',
    })
  })
})
