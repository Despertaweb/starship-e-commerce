// MODULE NAME: MUI

export const ui = {
  namespaced: true,
  state: {
    loading: {
      isLoading: false,
      msg: null,
    },
    snackbar: {
      text: null,
      show: false,
      timeout: 3000,
      color: null,
    },
  },
  mutations: {
    SET_LOADING(state, payload) {
      state.loading = payload
    },

    SET_SNACKBAR(state, payload) {
      state.snackbar = payload
    },
  },
}
