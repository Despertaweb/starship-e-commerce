// MODULE NAME:  MShoppingCart

export const shoppingCart = {
  namespaced: true,
  state: {
    items: [],
  },
  mutations: {
    PUSH_ITEM({ items: storedItems }, payload) {
      const index = storedItems.findIndex(item => payload.name == item.name)

      if (index === -1) {
        storedItems.push({ ...payload, count: 1 })
        return
      }

      const count = {
        count: storedItems[index].count++,
      }

      Object.assign({}, storedItems[index], count)
    },

    DELETE_ITEM({ items: storedItems }, payload) {
      const index = storedItems.findIndex(item => {
        return payload.name == item.name
      })

      storedItems.splice(index, 1)
    },

    REST_ITEM({ items: storedItems }, payload) {
      const index = storedItems.findIndex(item => payload.name == item.name)

      // REMOVE if is last item of the model
      if (payload.count === 1) {
        storedItems.splice(index, 1)
        return
      }

      const item = storedItems[index]

      // DECREASE count
      item.count--
      storedItems[index] = item
    },
  },
  getters: {
    itemsCount({ items: storedItems }) {
      return storedItems.length === 0
        ? 0
        : storedItems.reduce((total, item) => {
            return total + item.count || 0
          }, 0)
    },
  },
}
