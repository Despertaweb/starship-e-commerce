// MODULE NAME:  MShips

import { API } from '@/api/axios'

export const ships = {
  namespaced: true,
  actions: {
    getPage(context, { page }) {
      return API(`starships/?page=${page}`)
        .then(res => {
          return res
        })
        .catch(err => {
          return err.response
        })
    },
    getShip(context, id) {
      return API(`starships/${id}/`)
        .then(res => {
          return res
        })
        .catch(err => {
          return err.response
        })
    },
    getNested(context, { url, id }) {
      return API(`${url}/${id}/`)
        .then(res => {
          return res.data
        })
        .catch(err => {
          return err.response
        })
    },
  },
}
