import Vue from 'vue'
import Vuex from 'vuex'

import { ships } from './modules/ships'
import { ui } from './modules/ui'
import { shoppingCart } from './modules/shopping-cart'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    MUI: ui,
    MShips: ships,
    MShoppingCart: shoppingCart,
  },
})
