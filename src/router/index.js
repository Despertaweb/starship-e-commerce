import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/VHome.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/ships',
    name: 'ships',
    component: () =>
      import(/* webpackChunkName: "ships" */ '../views/VShips.vue'),
  },
  {
    path: '/ships/:id',
    name: 'ship',
    props: true,
    component: () =>
      import(/* webpackChunkName: "ship" */ '../views/VShip.vue'),
  },
  {
    path: '/cart',
    name: 'cart',
    props: true,
    component: () =>
      import(
        /* webpackChunkName: "ShoppingCart" */ '../views/VShoppingCart.vue'
      ),
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/VAbout.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
