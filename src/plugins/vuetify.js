import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import ca from 'vuetify/es5/locale/ca'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#ff8e30',
        secondary: '#006b5e',
        accent: '#009278',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      },
    },
  },
  lang: {
    locales: { ca },
    current: 'ca',
  },
  icons: {
    iconfont: 'fa',
  },
})
