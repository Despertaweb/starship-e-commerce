import axios from 'axios'

export const AXIOS = axios.create({
  baseURL: 'https://swapi.co/api/',
})

export function API(url, type = 'GET', responseType = 'json') {
  return AXIOS({
    method: type,
    url,
    responseType,
  })
}
