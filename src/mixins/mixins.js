import Vue from 'vue'
import { mapMutations } from 'vuex'

export const loadingMixin = Vue.mixin({
  methods: {
    ...mapMutations('MUI', ['SET_LOADING']),

    $setLoading(msg = 'Loading...') {
      const payload = {
        msg,
        isLoading: true,
      }
      this.SET_LOADING(payload)
    },
    $unsetLoading() {
      const payload = {
        isLoading: false,
        msg: null,
      }
      this.SET_LOADING(payload)
    },
  },
})

export const snackbarMixin = Vue.mixin({
  methods: {
    ...mapMutations('MUI', ['SET_SNACKBAR']),

    $setSnackbar(text, color = 'info', timeout = 3000) {
      const payload = {
        color,
        show: true,
        text,
        timeout,
      }
      this.SET_SNACKBAR(payload)

      setTimeout(() => {
        this.$unsetSnackbar()
      }, timeout)
    },
    $unsetSnackbar() {
      const payload = {
        color: null,
        text: null,
        show: false,
        timeout: 3,
      }
      this.SET_SNACKBAR(payload)
    },
  },
})
